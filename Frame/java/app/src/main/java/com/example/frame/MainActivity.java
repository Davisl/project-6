package com.example.frame;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.view.animation.Animation;

public class MainActivity extends AppCompatActivity {

    private AnimationDrawable birdAnimation;
    private ImageView birdImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        birdImage = (ImageView) findViewById(R.id.birdy);
        birdImage.setBackgroundResource(R.drawable.bird2);
        birdAnimation = (AnimationDrawable) birdImage.getBackground();

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {


        Handler myHandler = new Handler();
        myHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Animation startanimation = AnimationUtils.loadAnimation(getApplicationContext(),
                        R.anim.fadein_animation);
                birdImage.startAnimation(startanimation);
            }
        }, 50);
        birdAnimation.start();
        return super.onTouchEvent(event);
    }
}
